package com.geoquiz.utility;

import java.io.Serializable;
import java.util.Objects;

/**
 * A standard generic Pair<X,Y>.
 * 
 * @param <X>
 *              first element of pair.
 * @param <Y>
 *              second element of pair.
 */
public class Pair<X extends Serializable, Y extends Serializable> implements Serializable {
    private static final long serialVersionUID = -6610839650665030894L;
    private final X x;
    private final Y y;

    /**
     * @param x
     *            element x of pair.
     * @param y
     *            element y of pair.
     */
    public Pair(final X x, final Y y) {
        super();
        this.x = x;
        this.y = y;
    }

    /**
     * @return the element x of pair.
     */
    public X getX() {
        return x;
    }

    /**
     * @return the element y of pair.
     */
    public Y getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(x, pair.x) &&
                Objects.equals(y, pair.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Pair{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
