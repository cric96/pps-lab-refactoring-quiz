package com.geoquiz.view.core;

import com.geoquiz.view.utility.MediaPlayer;

public interface Screen<W> {
    MediaPlayer mediaPlayer();

    void showScene(W w);
}
