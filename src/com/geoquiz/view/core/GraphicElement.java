package com.geoquiz.view.core;

public interface GraphicElement<X> {

    X asLibraryRepresentation();
}
