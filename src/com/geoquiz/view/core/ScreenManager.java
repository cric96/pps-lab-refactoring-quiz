package com.geoquiz.view.core;

public interface ScreenManager<W> {
    <T extends Screen<W>> void changeScreen (Class<T> sceneClass);

    void exit();
}
