package com.geoquiz.view;

import com.geoquiz.model.quiz.Quiz;
import com.geoquiz.view.button.ButtonName;
import com.geoquiz.view.core.Screen;
import com.geoquiz.view.core.ScreenManager;
import com.geoquiz.view.menu.AbstractScreen;
import com.geoquiz.view.menu.QuizGamePlay;
import com.geoquiz.view.utility.ExitProgram;
import javafx.stage.Stage;

import java.util.*;

public class FXScreenManager implements ScreenManager<Stage> {
    private static final FXScreenManager SINGLETON = new FXScreenManager();
    private final Map<Class<?>, Screen<Stage>> cache = new HashMap<>();
    private final Set<Class<?>> screenNotCached = new HashSet<>(Arrays.asList(QuizGamePlay.class));
    private Stage mainStage;

    public static FXScreenManager instance() {
        return SINGLETON;
    }
    @Override
    public <T extends Screen<Stage>> void changeScreen(final Class<T> sceneClass) {
        Objects.requireNonNull(mainStage, "stage must be initialized");
        Objects.requireNonNull(sceneClass);
        changeScene(cacheScreenIfNotPresent(sceneClass));
    }

    @Override
    public void exit() {
        Objects.requireNonNull(mainStage);
        ExitProgram.exitProgram(mainStage);
    }

    public void initStage(final Stage mainStage) {
        Objects.requireNonNull(mainStage);
        this.mainStage = mainStage;
    }

    private <T extends Screen<Stage>> Screen<Stage> cacheScreenIfNotPresent(final Class<T> clazz) {
        try {
            if(screenNotCached.contains(clazz)) {
                return clazz.newInstance();
            }
            if(!cache.containsKey(clazz)) {
                cache.put(clazz,clazz.newInstance());
            }
            return cache.get(clazz);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace(); /*TODO*/
        }
        return null;
    }

    private void changeScene(final Screen<Stage> newScene) {
        Objects.requireNonNull(mainStage);
        newScene.showScene(mainStage);
    }
}
