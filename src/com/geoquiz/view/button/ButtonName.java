package com.geoquiz.view.button;

public interface ButtonName {
    static ButtonName of(String name) {
        return new ButtonName() {
            @Override
            public String toString() {
                return name;
            }
        };
    }
    static ButtonName none() {
        return NoNameButton.instance();
    }

}
