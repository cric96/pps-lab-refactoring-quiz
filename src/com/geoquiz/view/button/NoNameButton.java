package com.geoquiz.view.button;

class NoNameButton implements ButtonName {
    private static final NoNameButton INSTANCE = new NoNameButton();

    private NoNameButton() {}

    static ButtonName instance() {
        return INSTANCE;
    }
}
