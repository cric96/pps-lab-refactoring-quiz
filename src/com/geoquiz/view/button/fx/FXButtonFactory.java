package com.geoquiz.view.button.fx;

import com.geoquiz.view.button.GenericButton;
import com.geoquiz.view.button.ButtonName;
import javafx.scene.Node;
import javafx.scene.paint.Color;

/**
 * static factory for buttons inside menu.
 */
public final class FXButtonFactory {

    private FXButtonFactory() {
    }

    /**
     *
     * 
     * @param colorBackground
     *            the color of button background.
     * 
     * @param width
     *            the width of button.
     * 
     * @return the button.
     */
    public static GenericButton<Node> createButton(final ButtonName category, final Color colorBackground, final double width) {
        return new FXGenericButton(category, colorBackground, width);
    }

}
