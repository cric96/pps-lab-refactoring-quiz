package com.geoquiz.view.button;

public enum ButtonsGame implements ButtonName{
    FREEZE("FREEZE"), SKIP("SKIP"), FIFTY_FIFTY("50:50");

    private final String name;

    ButtonsGame(final String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }
}
