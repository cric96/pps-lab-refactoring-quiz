package com.geoquiz.view.label.fx;

import com.geoquiz.view.label.GenericLabel;
import javafx.scene.Node;
import javafx.scene.paint.Color;

/**
 * static factory for labels inside menu.
 */
public final class FXLabelFactory {

    private FXLabelFactory() {
    }

    /**
     * 
     * @param name
     *            the text of label.
     * @param color
     *            the color of label.
     * @param font
     *            the text font.
     * @return the label.
     */
    public static GenericLabel<Node> createLabel(final String name, final Color color, final double font) {
        return new FXGenericLabelImpl(name, color, font);
    }

}
