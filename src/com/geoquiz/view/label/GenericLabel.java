package com.geoquiz.view.label;

import com.geoquiz.view.core.GraphicElement;

/**
 * A label inside the menu.
 */
public interface GenericLabel<X> extends GraphicElement<X> {
    /**
     * set the text of label.
     * 
     * @param text
     *            the text of label.
     */
    void setText(String text);

    /**
     * Gets the text of the label.
     * 
     * @return the text o the label.
     */
    String getText();
}
