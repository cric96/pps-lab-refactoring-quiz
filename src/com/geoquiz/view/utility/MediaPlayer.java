package com.geoquiz.view.utility;

import com.sun.tracing.dtrace.FunctionName;

@FunctionalInterface
public interface MediaPlayer {
    void playSound();
}
