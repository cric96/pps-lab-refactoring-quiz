package com.geoquiz.view.menu;

import com.geoquiz.view.button.GenericButton;
import com.geoquiz.view.button.fx.FXButtonFactory;
import com.geoquiz.view.label.GenericLabel;
import com.geoquiz.view.utility.Background;
import com.geoquiz.view.utility.ConfirmBox;
import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.label.Labels;
import com.geoquiz.view.label.fx.FXLabelFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.geoquiz.controller.account.Account;
import com.geoquiz.controller.account.AccountImpl;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * The scene where user can choose difficulty level.
 */
public class LoginMenuScene extends AbstractScreen {

    private static final double TF_OPACITY = 0.7;
    private static final double TF_FONT = 25;
    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 525;
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_3_X = 450;
    private static final double POS_3_Y = 300;
    private static final double FONT = 35;
    private static final double BUTTON_WIDTH = 350;

    private final TextField tfUser = new TextField();
    private final PasswordField tfPass = new PasswordField();
    private final VBox vbox1 = new VBox();
    private final VBox vbox2 = new VBox(20);
    private final VBox vbox3 = new VBox(10);
    private boolean okLogin;
    private static String username;

    /**
     * @throws IOException
     *             for exception login.
     */
    public LoginMenuScene() throws IOException {

        final GenericLabel<Node> lblUser;
        final GenericLabel<Node> lblPass;

        final GenericButton<Node> btnEnter;
        final GenericButton<Node> btnNewPlayer;
        final GenericButton<Node> btnExit;


        btnExit = FXButtonFactory.createButton(Buttons.ESCI, Color.BLUE, BUTTON_WIDTH);
        btnEnter = FXButtonFactory.createButton(Buttons.ACCEDI, Color.BLUE, BUTTON_WIDTH);
        btnNewPlayer = FXButtonFactory.createButton(Buttons.REGISTRATI, Color.BLUE, BUTTON_WIDTH);

        lblUser = FXLabelFactory.createLabel(Labels.USERNAME.toString(), Color.WHITE, FONT);
        lblPass = FXLabelFactory.createLabel(Labels.PASSWORD.toString(), Color.WHITE, FONT);

        tfUser.getFont();
        tfUser.setFont(Font.font(TF_FONT));
        tfUser.setOpacity(TF_OPACITY);
        tfUser.setPromptText("Username");

        tfPass.getFont();
        tfPass.setFont(Font.font(TF_FONT));
        tfPass.setOpacity(TF_OPACITY);
        tfPass.setPromptText("Password");

        vbox1.setTranslateX(POS_1_X);
        vbox1.setTranslateY(POS_1_Y);
        vbox2.setTranslateX(POS_2_X);
        vbox2.setTranslateY(POS_2_Y);
        vbox3.setTranslateX(POS_3_X);
        vbox3.setTranslateY(POS_3_Y);

        vbox1.getChildren().addAll((Node) btnEnter, (Node) btnNewPlayer, (Node) btnExit);
        vbox2.getChildren().addAll((Node) lblUser, (Node) lblPass);
        vbox3.getChildren().addAll(tfUser, tfPass);

        ((Node) btnEnter).setOnMouseClicked(event -> {

            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            try {
                okLogin = true;
                getAccountOrClose().checkLogin(getUser(), getPass());
            } catch (IllegalArgumentException e) {
                okLogin = false;
                ConfirmBox.getBox();
                final Alert alert = ConfirmBox.getAlert("Errore! Username o password errati!", Color.BLACK);
                alert.show();
                e.printStackTrace();
            }
            if (okLogin) {
                username = tfUser.getText();
                manager().changeScreen(MainMenuScene.class);
            }

        });

        btnExit.asLibraryRepresentation().setOnMouseClicked(event -> {
            mediaPlayer().playSound();
            manager().exit();
        });

        ((Node) btnNewPlayer).setOnMouseClicked(event -> {

            if (!MainWindow.isWavDisabled()) {
                MainWindow.playClick();
            }
            manager().changeScreen(AccountRegisterScene.class);

        });
    }

    /**
     * @return username autentication.
     */
    public String getUser() {
        return tfUser.getText();
    }

    /**
     * @return password autentication.
     */
    public String getPass() {
        return tfPass.getText();
    }

    /**
     * @return username.
     */
    public static String getUsername() {
        return username;
    }

    @Override
    protected List<Node> nodesOnPane() {
        return Arrays.asList(Background.getImage(), Background.createBackground(), vbox1, vbox2, vbox3,
                Background.getLogo());
    }

    private Account getAccountOrClose() {
        try {
            return new AccountImpl("account.txt");
        } catch (IOException e) {
            manager().exit();
        }
        return null; //never arrive here
    }
}
