package com.geoquiz.view.menu;

import com.geoquiz.view.button.GenericButton;
import com.geoquiz.view.button.fx.FXButtonFactory;
import com.geoquiz.view.label.GenericLabel;
import com.geoquiz.view.utility.Background;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.label.fx.FXLabelFactory;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * The scene where user can change game options.
 */
public class OptionScene extends AbstractScreen {

    private static final double POS_2_X = 250;
    private static final double POS_O = 275;
    private static final double POS = 575;
    private static final double POS_1_X = 100;
    private static final double BUTTON_WIDTH = 350;

    private final Pane panel = new Pane();
    private final CheckBox sound = new CheckBox("MUSICA");
    private final CheckBox effect = new CheckBox("EFFETTI SONORI");
    private final VBox vbox = new VBox(sound, effect);
    private final VBox vbox2 = new VBox();

    public OptionScene() {
        super();


        final GenericButton<Node> back;
        final GenericButton<Node> save;
        back = FXButtonFactory.createButton(Buttons.INDIETRO, Color.BLUE, BUTTON_WIDTH);
        save = FXButtonFactory.createButton(Buttons.SALVA, Color.BLUE, BUTTON_WIDTH);

        sound.setSelected(!MainWindow.isMusicDisabled());
        sound.setStyle("-fx-font-size: 35");
        effect.setSelected(!MainWindow.isWavDisabled());
        effect.setStyle("-fx-font-size: 35");

        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_O);
        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS);
        vbox2.getChildren().addAll((Node) save, (Node) back);

        attachActionOnNodeWithSound(back, () -> manager().changeScreen(MainMenuScene.class));
        attachActionOnNodeWithSound(save, this::save);

    }

    private void save() {
        if (!sound.isSelected()) {
            MainWindow.disableMusic();
        } else {
            MainWindow.resumeMusic();
        }
        if (!effect.isSelected()) {
            MainWindow.stopClick();
        } else {
            MainWindow.playClick();
        }
    }

    @Override
    protected List<Node> nodesOnPane() {
        return Arrays.asList(Background.getImage(), Background.createBackground(), vbox, vbox2,
                Background.getLogo(), getUserLabel().asLibraryRepresentation());
    }
}
