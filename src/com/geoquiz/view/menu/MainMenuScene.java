package com.geoquiz.view.menu;

import com.geoquiz.controller.account.Account;
import com.geoquiz.controller.account.AccountImpl;
import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.GenericButton;
import com.geoquiz.view.button.fx.FXButtonFactory;
import com.geoquiz.view.utility.Background;
import javafx.scene.Node;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * The scene where user can choose how to do.
 */
public class MainMenuScene extends AbstractScreen {

    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double BUTTON_WIDTH = 350;
    private static final double POS_X_INSTRUCTIONS = 900;
    private static final double POS_Y_INSTRUCTIONS = 638;

    private final VBox vbox = new VBox();
    private final VBox instructionsButtonBox = new VBox();

    /**

     * @throws IOException
     *             for exception.
     */
    public MainMenuScene() throws IOException {
        final GenericButton<Node> play;
        final GenericButton<Node> ranking;
        final GenericButton<Node> options;
        final GenericButton<Node> exit;
        final GenericButton<Node> stats;
        final GenericButton<Node> instructions;
        final Account account = new AccountImpl("account.txt");

        play = FXButtonFactory.createButton(Buttons.GIOCA, Color.BLUE, BUTTON_WIDTH);
        ranking = FXButtonFactory.createButton(Buttons.CLASSIFICA, Color.BLUE, BUTTON_WIDTH);
        options = FXButtonFactory.createButton(Buttons.OPZIONI, Color.BLUE, BUTTON_WIDTH);
        exit = FXButtonFactory.createButton(Buttons.ESCI, Color.BLUE, BUTTON_WIDTH);
        stats = FXButtonFactory.createButton(Buttons.STATISTICHE, Color.BLUE, BUTTON_WIDTH);
        instructions = FXButtonFactory.createButton(Buttons.ISTRUZIONI, Color.BLUE, BUTTON_WIDTH);

        instructionsButtonBox.setTranslateX(POS_X_INSTRUCTIONS);
        instructionsButtonBox.setTranslateY(POS_Y_INSTRUCTIONS);
        instructionsButtonBox.getChildren().add((Node) instructions);

        vbox.setTranslateX(POS_1_X);
        vbox.setTranslateY(POS_1_Y);
        vbox.getChildren().addAll((Node) play, (Node) stats, (Node) ranking, (Node) options, (Node) exit);

        attachActionOnNodeWithSound(exit, () -> {
            account.logout();
            manager().exit();
        });
        attachActionOnNodeWithSound(instructions, () -> manager().changeScreen(InstructionScene.class));
        attachActionOnNodeWithSound(stats, () -> manager().changeScreen(MyRankingScene.class));
        attachActionOnNodeWithSound(ranking, () -> manager().changeScreen(AbsoluteRankingScene.class));
        attachActionOnNodeWithSound(options, () -> manager().changeScreen(OptionScene.class));
        attachActionOnNodeWithSound(play, () -> manager().changeScreen(CategoryScene.class));
    }

    @Override
    protected List<Node> nodesOnPane() {
        return Arrays.asList(Background.getImage(), Background.createBackground(), vbox,
                Background.getLogo(), getUserLabel().asLibraryRepresentation(), instructionsButtonBox);
    }
}
