package com.geoquiz.view.menu;

import com.geoquiz.view.button.ButtonName;
import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.ButtonsCategory;
import com.geoquiz.view.button.GenericButton;
import com.geoquiz.view.button.fx.FXButtonFactory;
import com.geoquiz.view.label.GenericLabel;
import com.geoquiz.view.label.fx.FXLabelFactory;
import com.geoquiz.view.utility.Background;
import javafx.scene.Node;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.List;

/**
 * The scene where user can choose difficulty level.
 */
public class LevelScene extends AbstractScreen {

    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;
    private static final double BUTTON_WIDTH = 350;
    private static final double USER_LABEL_FONT = 40;
    private final VBox vbox = new VBox();
    private final VBox vbox2 = new VBox();

    public LevelScene() {

        final GenericButton<Node> back;
        final GenericButton<Node> easy;
        final GenericButton<Node> medium;
        final GenericButton<Node> hard;

        back = FXButtonFactory.createButton(Buttons.INDIETRO, Color.BLUE, BUTTON_WIDTH);
        easy = FXButtonFactory.createButton(ButtonsCategory.FACILE, Color.BLUE, BUTTON_WIDTH);
        medium = FXButtonFactory.createButton(ButtonsCategory.MEDIO, Color.BLUE, BUTTON_WIDTH);
        hard = FXButtonFactory.createButton(ButtonsCategory.DIFFICILE, Color.BLUE, BUTTON_WIDTH);

        vbox.getChildren().addAll((Node) easy, (Node) medium, (Node) hard);
        vbox2.getChildren().add((Node) back);

        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);

        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS_Y_BACK);

        attachActionOnNodeWithSound(back, () -> manager().changeScreen(CategoryScene.class));
        attachActionOnNodeWithSound(easy, levelButtonAction(ButtonsCategory.FACILE));
        attachActionOnNodeWithSound(medium, levelButtonAction(ButtonsCategory.MEDIO));
        attachActionOnNodeWithSound(hard, levelButtonAction(ButtonsCategory.DIFFICILE));

    }

    private Runnable levelButtonAction(final ButtonName level) {
        return () -> {
            currentContext().setCurrentLevel(level);
            manager().changeScreen(QuizGamePlay.class);
        };
    }
    @Override
    protected List<Node> nodesOnPane() {
        return Arrays.asList(createBackgroundImage(), Background.createBackground(), vbox, vbox2,
                Background.getLogo(), getUserLabel().asLibraryRepresentation());
    }
}
