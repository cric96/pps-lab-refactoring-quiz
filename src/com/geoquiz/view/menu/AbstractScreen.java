package com.geoquiz.view.menu;

import com.geoquiz.view.FXScreenManager;
import com.geoquiz.view.button.ButtonName;
import com.geoquiz.view.button.ButtonsCategory;
import com.geoquiz.view.button.GenericButton;
import com.geoquiz.view.core.Screen;
import com.geoquiz.view.core.ScreenManager;
import com.geoquiz.view.label.GenericLabel;
import com.geoquiz.view.label.fx.FXLabelFactory;
import com.geoquiz.view.utility.Background;
import com.geoquiz.view.utility.MediaPlayer;
import com.geoquiz.view.utility.ScreenAdapter;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.List;

public abstract  class AbstractScreen extends Scene implements Screen<Stage> {
    private static final double OPACITY = 0.5;
    private static final double USER_LABEL_FONT = 40;
    private static final GameContext GAME_CONTEXT = new GameContext(); //Viola SRP
    private final ScreenManager<Stage> mainScreenManager = FXScreenManager.instance();
    private final Pane mainPane = new Pane();

    private static final MediaPlayer FX_MEDIA_PLAYER = () -> {
        if (!MainWindow.isWavDisabled()) {
            MainWindow.playClick();
        }
    };
    public AbstractScreen()
    {
        super(new StackPane(), ScreenAdapter.getScreenWidth(), ScreenAdapter.getScreenHeight());
        this.setRoot(mainPane);
    }

    protected void attachActionOnNodeWithSound(final GenericButton<Node> node, final Runnable action) {
        node.asLibraryRepresentation().setOnMouseClicked(e -> {
            mediaPlayer().playSound();
            action.run();
        });
    }

    public MediaPlayer mediaPlayer() {
        return FX_MEDIA_PLAYER;
    }

    protected void attachActionOnNode(final Node node, final Runnable action) {
        node.setOnMouseClicked(e -> action.run());
    }

    protected ScreenManager<Stage> manager() {
        return mainScreenManager;
    }

    @Override
    public void showScene(Stage stage) {
        mainPane.getChildren().clear();
        mainPane.getChildren().addAll(nodesOnPane());
        stage.setScene(this);
    }

    public ImageView createBackgroundImage() {
        final ButtonName currentCategory = currentContext().currentCategory;
        if (currentCategory == ButtonsCategory.CAPITALI) {
            return createImageView("/images/capitali.jpg");
        } else if (currentCategory == ButtonsCategory.MONUMENTI) {
            return createImageView("/images/monumenti.jpg");
        } else if (currentCategory == ButtonsCategory.VALUTE) {
            return createImageView("/images/valute.jpg");
        } else if (currentCategory == ButtonsCategory.CUCINA) {
            return createImageView("/images/cucina.jpg");
        } else if (currentCategory == ButtonsCategory.BANDIERE) {
            return createImageView("/images/bandiere.jpg");
        } else {
            return Background.getImage();
        }
    }

    private ImageView createImageView(final String name) {
        /* can implement caching here*/
        final ImageView img;
        img = Background.getCategoryImage(name);
        img.setOpacity(OPACITY);
        return img;
    }

    protected abstract List<Node> nodesOnPane();

    protected GenericLabel<Node> getUserLabel() {
        return FXLabelFactory.createLabel("USER: " + LoginMenuScene.getUsername(), Color.BLACK,
                USER_LABEL_FONT);
    }

    protected GameContext currentContext() {
        return GAME_CONTEXT;
    }

    protected static class GameContext {
        private ButtonName currentCategory = ButtonName.none();
        private ButtonName currentMode = ButtonName.none();
        private ButtonName currentLevel = ButtonName.none();

        ButtonName currentCategory() {
            return  currentCategory;
        }

        ButtonName currentMode() {
            return currentMode;
        }

        ButtonName currentLevel () {
            return currentLevel;
        }

        void setCategory(final ButtonName category) {
            this.currentCategory = category;
        }

        void setCurrentMode(final ButtonName mode) {
            this.currentMode = mode;
        }

        void setCurrentLevel(final ButtonName level) {
            this.currentLevel = level;
        }

    }
}
