package com.geoquiz.view.menu;

import com.geoquiz.view.button.ButtonName;
import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.ButtonsGame;
import com.geoquiz.view.button.GenericButton;
import com.geoquiz.view.button.fx.FXButtonFactory;
import com.geoquiz.view.label.GenericLabel;
import com.geoquiz.view.utility.Background;
import com.geoquiz.view.utility.ConfirmBox;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBException;

import com.geoquiz.controller.quiz.QuizController;
import com.geoquiz.controller.quiz.QuizControllerImpl;
import com.geoquiz.controller.quiz.TimeController;
import com.geoquiz.controller.ranking.Ranking;
import com.geoquiz.utility.Pair;
import com.geoquiz.view.label.fx.FXLabelFactory;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * The scene of game play.
 */
public class QuizGamePlay extends AbstractScreen {

    private static final Integer TIME = 10;
    private static final double POS_Y_BACK = 650;
    private static final double POS_1_X = 100;
    private static final double PB_OPACITY = 0.8;
    private static final double POS_PB_X = 200;
    private static final double POS_PB_Y = 50;
    private static final double PB_SIZE_X = 800;
    private static final double PB_SIZE_Y = 50;
    private static final double POS_X_ANSWERS = 350;
    private static final double POS_Y_ANSWERS = 300;
    private static final double POS_X_QUESTION = 350;
    private static final double POS_Y_QUESTION = 200;
    private static final double POS_X_HELPS_BOX = 850;
    private static final double POS_Y_HELPS_BOX = 550;
    private static final double QUESTION_FONT = 50;
    private static final double SCORE_FONT = 35;
    private static final double CATEGORY_FONT = 35;
    private static final double POS_X_SCORE_BOX = 1100;
    private static final double BUTTON_WIDTH = 350;
    private static final double HELPS_WIDTH = 400;
    private static final double ANSWERS_WIDTH = 550;
    private static final double POS_X_FLAG_IMAGE = 510;
    private static final double POS_Y_FLAG_IMAGE = 125;
    private static final double IMAGE_WIDTH = 250;
    private static final double IMAGE_HEIGHT = 150;
    private static final double POS_X_LIFE_BOX = 1100;
    private static final double POS_Y_LIFE_BOX = 50;
    private static final double POS_X_LABEL_BOX = 350;
    private static final double POS_Y_LABEL_BOX = 525;
    private static final double LABEL_FONT = 30;
    private static final double SLEEP = 1500;

    private final QuizController controller;
    private final TimeController timeController;
    private final Ranking ranking = Ranking.getInstance();

    private final Pane panel = new Pane();

    private final ProgressBar pb = new ProgressBar(0);
    private final IntegerProperty timeSeconds = new SimpleIntegerProperty(TIME * 100);
    private final javafx.scene.control.Label timerLabel = new javafx.scene.control.Label();
    private Timeline timeline;
    private Timeline freezeTimeline;
    private Timeline pauseTimeline;
    private final Text title = new Text(currentContext().currentCategory().toString());
    private final VBox backBox = new VBox();
    private final VBox hboxBar = new VBox(20, pb, timerLabel);
    private final VBox answersBox = new VBox(5.0);
    private final VBox questionBox = new VBox();
    private final VBox categoryBox = new VBox(title);
    private final javafx.scene.control.Label lifeLabel = new javafx.scene.control.Label();
    private List<GenericButton> answers;
    private final javafx.scene.control.Label scoreLabel = new javafx.scene.control.Label();
    private final GenericLabel questionGenericLabel;
    private ImageView flag;
    private final javafx.scene.control.Label label = new javafx.scene.control.Label();
    private boolean isAnswerCorrect;
    private boolean isFirstQuestion = true;

    /**
     * @throws JAXBException
     *             for xml exception.
     * @throws IOException
     *             for exception
     */
    public QuizGamePlay() throws JAXBException, IOException {

        this.controller = new QuizControllerImpl(currentContext().currentCategory().toString(), currentContext().currentMode().toString(),
                currentContext().currentLevel() == ButtonName.none() ? Optional.empty() : Optional.of(currentContext().currentLevel().toString()));

        this.timeController = new TimeController(controller);

        final GenericButton menu;

        title.setFont(Font.font(CATEGORY_FONT));
        menu = FXButtonFactory.createButton(Buttons.MENU, Color.BLUE, BUTTON_WIDTH);
        answers = this.createAnswersButtonList();
        questionGenericLabel = FXLabelFactory.createLabel(createQuestionLabel(), Color.BLACK, QUESTION_FONT);
        timerLabel.setTextFill(Color.RED);
        timerLabel.setStyle("-fx-font-size: 5em;");
        backBox.getChildren().addAll((Node) menu);

        ((Node) menu).setOnMouseClicked(event -> {
            if (timeline != null) {
                timeline.stop();
            }
            if (freezeTimeline != null) {
                freezeTimeline.stop();
            }
            if (pauseTimeline != null) {
                pauseTimeline.stop();
            }
            manager().changeScreen(CategoryScene.class);
        });

        timeStart();
        if (currentContext().currentCategory().toString().equals("BANDIERE")) {
            questionBox.setTranslateX(POS_X_FLAG_IMAGE);
            questionBox.setTranslateY(POS_Y_FLAG_IMAGE);
            flag = createQuestionImage();
            questionBox.getChildren().add(flag);
        } else {
            questionBox.setTranslateX(POS_X_QUESTION);
            questionBox.setTranslateY(POS_Y_QUESTION);
            questionBox.getChildren().add((Node) questionGenericLabel);
        }
        answers.forEach(a -> answersBox.getChildren().add((Node) a));

        answers.forEach(b -> ((Node) b).setOnMouseClicked(e -> {
            try {
                this.answersEventHandler(b);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }));

        backBox.setTranslateX(POS_1_X);
        backBox.setTranslateY(POS_Y_BACK);

        answersBox.setTranslateX(POS_X_ANSWERS);
        answersBox.setTranslateY(POS_Y_ANSWERS);

        hboxBar.setTranslateX(POS_PB_X);
        hboxBar.setTranslateY(POS_PB_Y);
        pb.setOpacity(PB_OPACITY);
        pb.setMinSize(PB_SIZE_X, PB_SIZE_Y);



        StackPane.setAlignment(answersBox, Pos.CENTER);
    }

    private List<GenericButton> createAnswersButtonList() {
        return this.controller.showAnswers().stream()
                .map(a -> FXButtonFactory.createButton(ButtonName.of(a), Color.BLUE, ANSWERS_WIDTH)).collect(Collectors.toList());
    }

    private String createQuestionLabel() {
        return this.controller.showStringQuestion();
    }

    private ImageView createQuestionImage() {
        final Image image = new Image(this.controller.showImageQuestion());
        final ImageView imageView = new ImageView(image);
        imageView.setFitWidth(IMAGE_WIDTH);
        imageView.setFitHeight(IMAGE_HEIGHT);
        return imageView;
    }

    private VBox createHelpBox() {
        final GenericButton freeze;
        final GenericButton skip;
        final GenericButton use5050;
        final VBox helpsBox = new VBox();

        freeze = FXButtonFactory.createButton(ButtonsGame.FREEZE, Color.RED, HELPS_WIDTH);
        skip = FXButtonFactory.createButton(ButtonsGame.SKIP, Color.RED, HELPS_WIDTH);
        use5050 = FXButtonFactory.createButton(ButtonsGame.FIFTY_FIFTY, Color.RED, HELPS_WIDTH);
        helpsBox.getChildren().addAll((Node) freeze, (Node) skip, (Node) use5050);
        helpsBox.setTranslateX(POS_X_HELPS_BOX);
        helpsBox.setTranslateY(POS_Y_HELPS_BOX);
        ((Node) freeze).setOnMouseClicked(e -> {
            if (this.controller.isFreezeAvailable()) {
                ((Node) freeze).setDisable(true);
                freeze.setText("Aiuto non più disponibile!");
                timeline.pause();
                freezeTimeline = new Timeline(
                        new KeyFrame(Duration.millis(this.controller.freeze()), a -> timeline.play()));
                freezeTimeline.playFromStart();
            }
        });
        ((Node) use5050).setOnMouseClicked(e -> {
            if (this.controller.is5050Available()) {
                this.answers.forEach(a -> {
                    if (this.controller.use5050().contains(a.getText())) {
                        a.setText("");
                        ((Node) a).setDisable(true);
                    }
                });
                ((Node) use5050).setDisable(true);
                use5050.setText("Aiuto non più disponibile!");
            }
        });
        ((Node) skip).setOnMouseClicked(e -> {
            if (this.controller.isSkipAvailable()) {
                this.controller.skip();
                timeline.stop();
                ((Node) skip).setDisable(true);
                skip.setText("Aiuto non più disponibile!");
                this.goToNextQuestion();
            }
        });
        return helpsBox;
    }

    private VBox createLabelBox() {
        final VBox labelBox = new VBox();
        labelBox.setTranslateX(POS_X_LABEL_BOX);
        labelBox.setTranslateY(POS_Y_LABEL_BOX);
        label.setText(this.getLabel());
        label.setFont(Font.font("Italic", FontWeight.BOLD, LABEL_FONT));
        labelBox.getChildren().add(label);
        return labelBox;
    }

    private VBox createLifeBox() {
        final VBox lifeBox = new VBox();
        lifeBox.setTranslateX(POS_X_LIFE_BOX);
        lifeBox.setTranslateY(POS_Y_LIFE_BOX);
        lifeLabel.setText("LIFE: " + this.getCurrentLife());
        lifeLabel.setFont(Font.font(SCORE_FONT));
        lifeBox.getChildren().add(lifeLabel);
        return lifeBox;
    }

    private VBox createScoreBox() {
        final VBox scoreBox = new VBox();
        scoreBox.setTranslateX(POS_X_SCORE_BOX);
        scoreLabel.setText("SCORE: " + this.getCurrentScore());
        scoreLabel.setFont(Font.font(SCORE_FONT));
        scoreBox.getChildren().add(scoreLabel);
        return scoreBox;
    }

    private int getCurrentLife() {
        return this.controller.getRemainingLives();
    }

    private int getCurrentScore() {
        return this.controller.getScore();
    }

    private String getLabel() {
        if (isFirstQuestion) {
            isFirstQuestion = false;
            return "";
        } else {
            label.setTextFill(isAnswerCorrect ? Color.GREEN : Color.RED);
            return isAnswerCorrect ? "RISPOSTA CORRETTA!" : "RISPOSTA ERRATA!";
        }
    }

    private void showCorrectAnswer() {
        answers.stream().filter(a -> a.getText().equals(this.controller.getCorrectAnswer())).findFirst().get()
                .setBackground(Color.GREEN);
    }

    private void answersEventHandler(final GenericButton b) throws InterruptedException {
        this.controller.hitAnswer(Optional.of(b.getText()));
        answers.forEach(a -> {
            ((Node) a).setDisable(true);
        });
        if (this.controller.checkAnswer()) {
            ((Node) b).setOnMouseExited(e -> {
                b.setBackground(Color.GREEN);
            });

            b.setBackground(Color.GREEN);
            isAnswerCorrect = true;
        } else {
            showCorrectAnswer();
            ((Node) b).setOnMouseExited(e -> {
                b.setBackground(Color.RED);
            });
            b.setBackground(Color.RED);
            isAnswerCorrect = false;
        }
        label.setText(getLabel());
        timeline.stop();
        if (freezeTimeline != null) {
            freezeTimeline.stop();
        }
        pauseTimeline = new Timeline(new KeyFrame(Duration.millis(SLEEP), e -> this.goToNextQuestion()));
        pauseTimeline.playFromStart();
    }

    private Pair<String, String> savePair() {
        /*todo check*/
        final ButtonName currentCategory = currentContext().currentCategory();
        if (title.getText().equals("CAPITALI") || title.getText().equals("MONUMENTI")) {
            if (currentCategory.toString().equals("CLASSICA")) {
                return new Pair<>(currentCategory.toString(),currentContext().currentLevel().toString());
            } else {
                return new Pair<>(currentCategory.toString(), currentContext().currentMode().toString());
            }
        } else {
            return new Pair<>(currentCategory.toString(), currentContext().currentMode().toString());
        }
    }

    private String savePlayer() {
        return LoginMenuScene.getUsername();
    }

    private void timeStart() {
        if (timeline != null) {
            timeline.stop();
        }
        timerLabel.textProperty().bind(timeSeconds.divide(100).asString());
        pb.progressProperty().bind(timeSeconds.divide((this.timeController.getTime()) * 100.0));
        timeSeconds.set((this.timeController.getTime() + 1) * 100);
        timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(this.timeController.getTime() + 1), e -> {
            showCorrectAnswer();
            isAnswerCorrect = false;
            label.setText(getLabel());
            this.controller.hitAnswer(Optional.empty());
            pauseTimeline = new Timeline(new KeyFrame(Duration.millis(SLEEP), k -> this.goToNextQuestion()));
            pauseTimeline.playFromStart();
        }, new KeyValue(timeSeconds, 0)));
        timeline.playFromStart();

    }

    private void gameOver() {
        final Alert alert = ConfirmBox.getAlert("Il gioco è terminato! SCORE: " + this.getCurrentScore(), Color.BLACK);
        alert.show();
        manager().changeScreen(CategoryScene.class);
    }

    private void goToNextQuestion() {
        if (pauseTimeline != null) {
            pauseTimeline.stop();
        }
        if (freezeTimeline != null) {
            freezeTimeline.stop();
        }
        label.setText("");
        timeline.stop();
        if (!this.controller.gameOver()) {
            this.controller.nextQuestion();
            if (currentContext().currentCategory().toString().equals("BANDIERE")) {
                flag.setImage(new Image(this.controller.showImageQuestion()));
            } else {
                questionGenericLabel.setText(this.createQuestionLabel());
            }
            if (!currentContext().currentMode().toString().equals("ALLENAMENTO")) {
                scoreLabel.setText("SCORE: " + getCurrentScore());
                lifeLabel.setText("LIFE: " + getCurrentLife());
            }
            answers = this.createAnswersButtonList();
            answers.forEach(c -> ((Node) c).setOnMouseClicked(e -> {
                try {
                    this.answersEventHandler(c);

                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }));
            answersBox.getChildren().clear();
            answers.forEach(a -> answersBox.getChildren().add((Node) a));
            timeStart();
        } else {
            try {
                this.ranking.save(savePlayer(), savePair(), getCurrentScore());

            } catch (ClassNotFoundException | IOException e1) {
                e1.printStackTrace();
            }
            this.gameOver();
        }
    }

    @Override
    protected List<Node> nodesOnPane() {
        if (currentContext().currentMode().toString().equals("CLASSICA")) {
            return Arrays.asList(createBackgroundImage(), Background.createBackground(), backBox,
                    hboxBar, answersBox, questionBox, createHelpBox(), categoryBox, createScoreBox(), createLifeBox());
        } else if (currentContext().currentMode().toString().equals("ALLENAMENTO")) {
            return Arrays.asList(createBackgroundImage(), Background.createBackground(), backBox,
                    hboxBar, answersBox, questionBox, categoryBox, createLabelBox());
        } else {
            return Arrays.asList(createBackgroundImage(), Background.createBackground(), backBox,
                    hboxBar, answersBox, questionBox, categoryBox, createScoreBox(), createLifeBox(), createLabelBox());
        }
    }
}
