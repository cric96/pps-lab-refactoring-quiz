package com.geoquiz.view.menu;

import com.geoquiz.view.button.ButtonName;
import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.ButtonsCategory;
import com.geoquiz.view.button.GenericButton;
import com.geoquiz.view.button.fx.FXButtonFactory;
import com.geoquiz.view.utility.Background;
import javafx.scene.Node;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.List;

/**
 * The scene where user can choose game modality.
 */
public class ModeScene extends AbstractScreen {
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 350;
    private static final double POS_3_Y = 200;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;
    private static final double LABEL_FONT = 35;
    private static final double BUTTON_WIDTH = 350;

    private final VBox vbox = new VBox();
    private final VBox vbox2 = new VBox();
    private final VBox vbox3 = new VBox();
    private final GenericButton<Node> back;
    private final GenericButton<Node> classic;
    private final GenericButton<Node> challenge;
    private final GenericButton<Node> training;

    private final javafx.scene.control.Label title = new javafx.scene.control.Label();

    public ModeScene() {

        back = FXButtonFactory.createButton(Buttons.INDIETRO, Color.BLUE, BUTTON_WIDTH);
        classic = FXButtonFactory.createButton(ButtonsCategory.CLASSICA, Color.BLUE, BUTTON_WIDTH);
        challenge = FXButtonFactory.createButton(ButtonsCategory.SFIDA, Color.BLUE, BUTTON_WIDTH);
        training = FXButtonFactory.createButton(ButtonsCategory.ALLENAMENTO, Color.BLUE, BUTTON_WIDTH);

        initializeButtons();

        title.setFont(Font.font("Italic", FontWeight.BOLD, LABEL_FONT));

        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);
        vbox.getChildren().addAll((Node) classic, (Node) challenge, (Node) training);
        vbox2.getChildren().add((Node) back);
        vbox3.getChildren().add(title);

        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS_Y_BACK);
        vbox3.setTranslateX(POS_2_X);
        vbox3.setTranslateY(POS_3_Y);

    }

    private void initializeButtons() {
        attachActionOnNodeWithSound(back, () -> manager().changeScreen(CategoryScene.class));
        attachActionOnNodeWithSound(classic, createActionOf(ButtonsCategory.CLASSICA));
        attachActionOnNodeWithSound(challenge, createActionOf(ButtonsCategory.SFIDA));
        attachActionOnNodeWithSound(training, createActionOf(ButtonsCategory.ALLENAMENTO));
    }

    private Runnable createActionOf(ButtonName category){
        if(category == ButtonsCategory.CLASSICA) {
            return createClassic();
        } else {
            return createStandard(category);
        }
    }

    private Runnable createClassic() {
        return () -> {
            currentContext().setCurrentMode(ButtonsCategory.CLASSICA);
            final ButtonName currentCategory = currentContext().currentCategory();
            if (currentCategory == ButtonsCategory.CAPITALI
                    || currentCategory == ButtonsCategory.MONUMENTI) {
                manager().changeScreen(LevelScene.class);
            } else {
                manager().changeScreen(QuizGamePlay.class);
            }
        };
    }

    private Runnable createStandard(ButtonName category) {
        return () -> {
            currentContext().setCurrentMode(category);
            manager().changeScreen(QuizGamePlay.class);
        };
    }
    @Override
    public void showScene(Stage stage) {
        refreshLabel();
        super.showScene(stage);
    }

    @Override
    protected List<Node> nodesOnPane() {
        return Arrays.asList(createBackgroundImage(), Background.createBackground(),
                Background.getLogo(), vbox, vbox2, vbox3,getUserLabel().asLibraryRepresentation());
    }

    private void refreshLabel() {
        final ButtonName currentCategory = currentContext().currentCategory();
        if (currentCategory == ButtonsCategory.CAPITALI) {
            title.setText("Sai indicare la capitale di ciascun paese?\nScegli prima la modalità di gioco!");
        } else if (currentCategory == ButtonsCategory.MONUMENTI) {
            title.setText(
                    "Sai indicacare dove si trovano questi famosi monumenti?\nScegli prima la modalità di gioco!");
        } else if (currentCategory == ButtonsCategory.BANDIERE) {
            title.setText("Sai indicare i paesi in base alla bandiera nazionale?\nScegli prima la modalità di gioco!");
        } else if (currentCategory == ButtonsCategory.CUCINA) {
            title.setText("Sai indicare di quali paesi sono tipici questi piatti?\nScegli prima la modalità di gioco!");
        } else if (currentCategory == ButtonsCategory.VALUTE) {
            title.setText(
                    "Sai indicare qual e' la valuta adottata da ciascun paese?\nScegli prima la modalità di gioco!");
        }
    }
}
