package com.geoquiz.view.menu;

import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.ButtonsCategory;
import com.geoquiz.view.button.GenericButton;
import com.geoquiz.view.button.fx.FXButtonFactory;
import com.geoquiz.view.layout.Spacing;
import com.geoquiz.view.utility.Background;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.util.Arrays;
import java.util.List;

/**
 * The scene where user can choose game category.
 */
public class CategoryScene extends AbstractScreen {
    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_X_BACK = 450;
    private static final double POS_Y_BACK = 600;
    private static final double BUTTON_WIDTH = 350;
    private static final double USER_LABEL_FONT = 40;

    private GenericButton<Node> capitals;
    private GenericButton<Node> currencies;
    private GenericButton<Node> dishes;
    private GenericButton<Node> monuments;
    private GenericButton<Node> flags;
    private GenericButton<Node> back;
    private final HBox middlePane = new HBox(Spacing.SMALL.getSpace());
    private final HBox topPane = new HBox(Spacing.SMALL.getSpace());
    private final VBox backPane = new VBox();

    public CategoryScene() {

        this.initializePanes();
        this.initializeButtons();
        this.populatePanes();
    }

    @Override
    protected List<Node> nodesOnPane() {
        return Arrays.asList(Background.getImage(), Background.createBackground(), middlePane, topPane, backPane,
                Background.getLogo(), getUserLabel().asLibraryRepresentation());
    }

    private void initializeButtons() {
        capitals = FXButtonFactory.createButton(ButtonsCategory.CAPITALI, Color.BLUE, BUTTON_WIDTH);
        currencies = FXButtonFactory.createButton(ButtonsCategory.VALUTE, Color.BLUE, BUTTON_WIDTH);
        dishes = FXButtonFactory.createButton(ButtonsCategory.CUCINA, Color.BLUE, BUTTON_WIDTH);
        monuments = FXButtonFactory.createButton(ButtonsCategory.MONUMENTI, Color.BLUE, BUTTON_WIDTH);
        flags = FXButtonFactory.createButton(ButtonsCategory.BANDIERE, Color.BLUE, BUTTON_WIDTH);
        back = FXButtonFactory.createButton(Buttons.INDIETRO, Color.BLUE, BUTTON_WIDTH);
        this.putListenerOnButtons();
    }

    private void putListenerOnButtons() {
        this.attachActionOnNodeWithSound(back, () -> manager().changeScreen(MainMenuScene.class));
        Arrays.asList(capitals,currencies,dishes,monuments,flags).forEach(this::addStandardListener);
    }

    private void initializePanes() {
        middlePane.setTranslateX(POS_1_X);
        middlePane.setTranslateY(POS_1_Y);

        topPane.setTranslateX(POS_2_X);
        topPane.setTranslateY(POS_2_Y);

        backPane.setTranslateX(POS_X_BACK);
        backPane.setTranslateY(POS_Y_BACK);
    }

    private void populatePanes() {
        middlePane.getChildren().addAll(flags.asLibraryRepresentation(), currencies.asLibraryRepresentation(), dishes.asLibraryRepresentation());
        topPane.getChildren().addAll(monuments.asLibraryRepresentation(), capitals.asLibraryRepresentation());
        backPane.getChildren().add((back.asLibraryRepresentation()));
    }

    private void addStandardListener(GenericButton<Node> genericButton) {
        this.attachActionOnNodeWithSound(genericButton, () -> {
            currentContext().setCategory(genericButton.name());
            manager().changeScreen(ModeScene.class);
        });
    }
}
