package com.geoquiz.view.layout;

public enum Spacing {
    SMALL(10), MEDIUM(15), BIG(20);
    private final double size;

    Spacing(final double size) {
        this.size = size;
    }

    public double getSpace() {
        return this.size;
    }
}
